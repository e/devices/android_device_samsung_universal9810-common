/*
 * Copyright (C) 2021, The LineageOS Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <android-base/properties.h>

#include "edify/expr.h"
#include "otautil/error_code.h"

Value *CompareVariant(const char *name, State *state,
                      const std::vector<std::unique_ptr<Expr>> &argv) {
  int ret = 0;
  std::string bootloader = android::base::GetProperty("ro.boot.bootloader", "");
  if (bootloader.empty()) {
    return ErrorAbort(state, kFileGetPropFailure,
                      "%s() failed to read current bootloader version", name);
  }

  std::string target_variant_arg;
  if (argv.empty() || !Evaluate(state, argv[0], &target_variant_arg)) {
    return ErrorAbort(state, kArgsParsingFailure,
                      "%s() error parsing arguments", name);
  }

  if (target_variant_arg.compare(
          bootloader.substr(4, target_variant_arg.length())) == 0) {
    ret = 1;
  }

  return StringValue(std::to_string(ret));
}

/**
 * for more information
 * see
 * https://android.stackexchange.com/questions/202491/what-do-the-numbers-and-letters-in-the-samsung-firmware-mean/202494#202494
 */
int CompareBootloader(std::string a, std::string b) {
  /* compare bootloader version */
  if (std::stoi(a.substr(a.length() - 5), nullptr, 32) <
      std::stoi(b.substr(b.length() - 5), nullptr, 32)) {
    return 1;
  }

  // if version on the device is the same or newer dont upgrade
  return 0;
}

Value *VerifyBootloaderMin(const char *name, State *state,
                           const std::vector<std::unique_ptr<Expr>> &argv) {
  int ret = 0;
  std::string bootloader = android::base::GetProperty("ro.boot.bootloader", "");
  if (bootloader.empty()) {
    return ErrorAbort(state, kFileGetPropFailure,
                      "%s() failed to read current bootloader version", name);
  }

  bool isCrown = bootloader.substr(0, 4).compare("N960") == 0;
  bool isStar2 = bootloader.substr(0, 4).compare("G965") == 0;

  if (bootloader.substr(4, 1).compare("F") == 0) {
    ret = CompareBootloader(
        bootloader, isCrown ? "N960FXXS9FVB1"
                            : (isStar2 ? "G965FXXUHFVB4" : "G960FXXUHFVB4"));
  } else if (bootloader.substr(4, 1).compare("N") == 0) {
    ret = CompareBootloader(
        bootloader, isCrown ? "N960NKSU3FVA1"
                            : (isStar2 ? "G965NKSU5FVA2" : "G960NKSU5FVA2"));
  }

  return StringValue(std::to_string(ret));
}

Value *VerifyBootloaderModel(const char *name, State *state,
                             const std::vector<std::unique_ptr<Expr>> &argv) {
  int ret = 0;
  std::string bootloader = android::base::GetProperty("ro.boot.bootloader", "");
  if (bootloader.empty()) {
    return ErrorAbort(state, kFileGetPropFailure,
                      "%s() failed to read current bootloader version", name);
  }

  if (bootloader.substr(4, 1).compare("F") == 0 ||
      bootloader.substr(4, 1).compare("N") == 0) {
    ret = 1;
  }

  return StringValue(std::to_string(ret));
}

void Register_librecovery_updater_universal9810() {
  RegisterFunction("universal9810.compare_variant", CompareVariant);
  RegisterFunction("universal9810.verify_bootloader_min", VerifyBootloaderMin);
  RegisterFunction("universal9810.verify_bootloader_model",
                   VerifyBootloaderModel);
}
