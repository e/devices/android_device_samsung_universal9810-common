#!/system/bin/sh

MOVED="/data/vendor/biometrics/moved"

if [ ! -f "$MOVED" ]; then
  mkdir -p /data/vendor/biometrics
  restorecon -R /data/vendor/biometrics
  cp -r /data/biometrics/* /data/vendor/biometrics/
  restorecon -R /data/vendor/biometrics
  echo 1 > $MOVED
fi
